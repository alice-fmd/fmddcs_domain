// -*- mode: C++ -*-
#ifndef FMD_DOMAIN_H
#define FMD_DOMAIN_H
#include <smixx/smirtl.hxx>
#include <algorithm>
#include <iterator>
#include <cctype>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include "RCU_client.h"
#include "MINICONF_client.h"

//____________________________________________________________________
namespace
{
  struct to_lower
  {
    char operator()(char c) const { return std::tolower(c); }
  };
}

//____________________________________________________________________
class FMD_domain : public SmiProxy
{
public:
  FMD_domain(const std::string& server, char* object, char* domain);
  ~FMD_domain();
  void Update();
  void smiCommandHandler();
public:
  void HandleConfigure();
  void HandleGoOff()     { _miniconf.Reset(); }
  void HandleGoStandby() { _miniconf.Reset(); }
  void HandleReset();
  void ParseOption(std::stringstream& args, 
		   const char* option, 
		   const char* value,
		   bool        def=true);
  void ParseOver(std::stringstream& args, 
		 const char* value);
  void ParseCards(std::stringstream& args, 
		  const char*  value,
		  unsigned int def=0x10000);
  RCU_client _rcu;
  MINICONF_client _miniconf;
};

//____________________________________________________________________
inline
FMD_domain::FMD_domain(const std::string& server, char* object, char* domain)
  : SmiProxy(object), _rcu(server, *this), _miniconf(server, *this)
{
  attach(domain);
  setVolatile();
  // setPrintOff();
  Update();
  // setParameter("RUN_TYPE", "DEFAULT");
  // setState("OFF");
}

//____________________________________________________________________
inline
FMD_domain::~FMD_domain()
{
  std::cout << "We're terminating" << std::endl;
}

//____________________________________________________________________
inline void 
FMD_domain::Update()
{
  
  std::string next = "OFF";
  if (_rcu.State() == RCU_client::Error || 
      _miniconf.State() == MINICONF_client::Error) 
    next = "ERROR";
  else {
    switch (_miniconf.State()) { 
    case MINICONF_client::NoLink: 
      if (_rcu.State() != RCU_client::NoLink) next = "ERROR";
      else                                    next = "OFF";
      break;
    case MINICONF_client::Idle:
      switch (_rcu.State()) { 
      case RCU_client::Idle:         next = "IDLE";    break;
      case RCU_client::Standby:      next = "STANDBY"; break;
      case RCU_client::Downloading:  next = "ERROR";   break;
      case RCU_client::Ready:        next = "READY";   break;
      case RCU_client::Running:      next = "READY";   break;
      }
      break;
    case MINICONF_client::Configuring:
      switch (_rcu.State()) { 
      case RCU_client::Idle:         next = "DOWNLOADING"; break;
      case RCU_client::Standby:      next = "DOWNLOADING"; break;
      case RCU_client::Downloading:  next = "DOWNLOADING"; break;
      case RCU_client::Ready:        next = "DOWNLOADING"; break;
      default:                       next = "ERROR";       break;
      }
    }
  }
  Info("Update proxy state (RCU=%s, MINICONF=%s) -> %s",
       _rcu.State2Text(), _miniconf.State2Text(), next.c_str());
  setState(const_cast<char*>(next.c_str()));
  if (next == "ERROR") 
    Error("Proxy is in an error state!");
}
//____________________________________________________________________
inline void 
FMD_domain::smiCommandHandler()
{
  printDateTime();
  Info("Got action %s", getAction());
  if      (testAction("GO_OFF"))      HandleGoOff();  
  else if (testAction("GO_STANDBY"))  HandleGoStandby();
  else if (testAction("RESET"))       HandleReset(); 
  else if (testAction("CONFIGURE"))   HandleConfigure();
}

//____________________________________________________________________
inline void 
FMD_domain::ParseOption(std::stringstream& args, 
			const char* option, 
			const char* value, 
			bool def)
{
  std::string val(value);
  std::transform(val.begin(),val.end(),val.begin(),to_lower());
  bool use = false;
  if      (val == "yes"       || val == "true") use = true;
  else if (val == "undefined" || val == "default") use = def;
  if (use) args << ";" << option;
}

//____________________________________________________________________
template <typename T>
T str2val(const char* str, T& v)
{
  std::stringstream s(str);
  s >> v;
  return v;
}

//____________________________________________________________________
inline void 
FMD_domain::ParseOver(std::stringstream& args, 
		      const char* value)
{
  std::string val(value);
  std::transform(val.begin(),val.end(),val.begin(),to_lower());
  if (val == "undefined" && val == "default") return;
  if      (val == "1") args << ";one";
  else if (val == "2") args << ";two";
}

//____________________________________________________________________
inline void 
FMD_domain::ParseCards(std::stringstream& args, 
		       const char* value, 
		       unsigned int def)
{
  std::string val(value);
  std::transform(val.begin(),val.end(),val.begin(),to_lower());
  if (val.empty() || val == "undefined" || val == "default") { 
    args << "0x" << std::hex << def;
    return;
  }
  std::stringstream l(val);
  unsigned int mask = 0;
  while (!l.eof()) { 
    std::string sc;
    std::getline(l, sc, ',');
    if (sc.empty()) continue;
    std::stringstream ssc(sc);
    unsigned int addr = 0;
    if (sc[0] == '0') { 
      ssc << std::oct;
      if (sc[1] == 'x') { 
	ssc << std::hex;
      }
    }
    ssc >> addr;
    mask |= (1 << addr);
  }
  Info("Front-end cards to enable %s (0x%08x)", val.c_str(), mask);
  args << "0x" << std::hex << mask << std::dec;
}

//____________________________________________________________________
inline void 
FMD_domain::HandleConfigure()
{
  std::string tag(getParameterString("RUN_TYPE"));
  std::transform(tag.begin(),tag.end(),tag.begin(),to_lower());
  if (tag.find("default")   != std::string::npos || 
      tag.find("undefined") != std::string::npos) 
    tag = "standalone";

  std::stringstream args;
  args << tag << ";";
  ParseCards(args, getParameterString("CARDS"), 0x10000);
  ParseOption(args, "sod",       getParameterString("SOD"),       false);
  ParseOption(args, "monitor",   getParameterString("MONITOR"),   true);
  ParseOption(args, "interrupt", getParameterString("INTERRUPT"), false);
  ParseOption(args, "trigger",   getParameterString("TRIGGER"),   true);
  ParseOption(args, "zero",      getParameterString("ZERO"),      false);
  ParseOver(args, getParameterString("OVER"));

  std::string a = args.str();

  Info("Sending configuration parameters \"%s\" to MINICONF", a.c_str());
  _miniconf.SendCommand(a);
}  


//____________________________________________________________________
inline void 
FMD_domain::HandleReset()
{
  if (_miniconf.State() == MINICONF_client::Error) 
    _miniconf.Clear();
  else 
    _miniconf.Reset();
}

#endif
//
// EOF
//
