#
#
#
ifeq ($(NO_LOG),)
INFOLOG_CPPFLAGS= -DUSE_INFOLOGGER
INFOLOG_LIBS	= /date/infoLogger/Linux/libInfo.a
endif
CPP		= g++ -E
CPPFLAGS	= $(shell dim-config --cppflags) 	\
		  $(shell smixx-config --cppflags)	\
		  $(INFOLOG_CPPFLAGS)
CXX		= g++ -c -g 
CXXFLAGS	= $(shell dim-config --cflags) 	
LD		= g++ 
LDFLAGS		= $(shell dim-config --ldflags) 	\
		  $(shell smixx-config --ldflags)	\
		  $(shell dim-config --cflags) 	
LIBS		= $(INFOLOG_LIBS)			\
		  $(shell dim-config --libs) 		\
		  $(shell smixx-config --libs)		



%.o:%.cxx
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< 

%:%.o
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

%.sobj:%.smi
	smiTrans $< 

all:	FMD_domain FMD_domain.sobj

FMD_domain.o:FMD_domain.cxx FMD_domain.h RCU_client.h MINICONF_client.h

run:	all
	./FMD_domain & 
	@sleep 1
	smiSM FMD FMD_domain.sobj & 
	@sleep 1
	smiGUI FMD 
	@sleep 1
	-killall -9 FMD_domain 
	@pid=`ps -o pid,cmd -C smiSM | grep FMD_domain.sobj | cut -f1 -d' '` ;\
		echo "Killing $$pid" ; \
		test "x$$pid" = "x" || kill -9 $$pid

install: all
	sudo cp FMD_domain /ecs/DCS/FMD
	sudo cp FMD_domain.smi /ecs/DCS/FMD/smifiles/
	sudo cp FMD_domain.sobj FMD_domain.smi /opt/ecs_site/DCS/FMD/
	sudo cp fmd.smipp /ecs/ECS/smifiles/

clean:
	rm -f *.o *~ *.sobj FMD_domain

#
# EOF
#
