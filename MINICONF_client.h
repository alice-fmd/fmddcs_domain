// -*- mode: C++ -*-
#ifndef MINICONF_CLIENT_H
#define MINICONF_CLIENT_H
#include <dim/dic.hxx>
#include <string>
class FMD_domain;

void Info(const char* msg, ...); 
void Error(const char* msg, ...);
void Fatal(const char* msg, ...);

//____________________________________________________________________
class MINICONF_client : public DimStampedInfo
{
public:
  enum State_t { 
    NoLink,
    Idle, 
    Configuring, 
    Error
  };
  MINICONF_client(const std::string& server, FMD_domain& domain);
  const char* State2Text();
  State_t State() const { return _value; }
  void infoHandler();
  bool SendCommand(const std::string& args);
  bool Reset() { return SendCommand("reset"); }
  bool Clear() { return SendCommand("clear"); }
  bool Default() { 
    return SendCommand("standalone;0x30000;monitor;sod;trigger");
  }
protected:
  static const char* MakeStateName(const std::string& server);
  static const char* MakeCommandName(const std::string& server);
  static const char* MakeMessageName(const std::string& server);
  void HandleState();
  void HandleMessage();
  DimStampedInfo _state;
  DimStampedInfo _msg;
  State_t        _value;
  std::string    _goName;
  FMD_domain&    _domain;
};

//____________________________________________________________________
#elif defined(ONLY_IMPLEMENTATION)
#include "FMD_domain.h"
#include <sstream>

inline
MINICONF_client::MINICONF_client(const std::string& server, FMD_domain& domain)
  : _state(MakeStateName(server), 0, 0, 0, this),
    _msg(MakeMessageName(server), 0, 0, 0, this),
    _goName(MakeCommandName(server)),
    _domain(domain)
{
}

//____________________________________________________________________
inline const char* 
MINICONF_client::State2Text()
{
  switch (_value) {
  case Idle:        return "IDLE";
  case Configuring: return "CONFIGURING";
  case Error:       return "ERROR";
  }
  return "No link";
}
//____________________________________________________________________
inline void 
MINICONF_client::infoHandler()
{
  DimInfo* info = getInfo();
  if      (!info) return;
  else if (info == &_state) HandleState();
  else if (info == &_msg)   HandleMessage();
}

//____________________________________________________________________
inline void 
MINICONF_client::HandleState()
{
  void*       data   = _state.getData();
  int         size   = _state.getSize();
  bool        nolink =  (!data || size <= 0);
  std::string s("nolink");
  if (!nolink) s = _state.getString();
  if      (std::string::npos != s.find("nolink"))      _value = NoLink;
  else if (std::string::npos != s.find("idle"))        _value = Idle;
  else if (std::string::npos != s.find("configuring")) _value = Configuring;
  else if (std::string::npos != s.find("error"))       _value = Error;
  else                                                 _value = Error;

  _domain.Update();
}

//____________________________________________________________________
inline void 
MINICONF_client::HandleMessage()
{
  void*          data   = _msg.getData();
  int            size   = _msg.getSize();
  bool           nolink =  (!data || size <= 0);
  if (nolink) return;
  unsigned short level;
  memcpy(&level, data, sizeof(short));
  char*          cdata  = reinterpret_cast<char*>(data);
  char*          msg    = &(cdata[sizeof(short)]);
    
  switch (level) { 
  case 1:  ::Error("Miniconf says: %s", msg); break;
  case 2:  ::Fatal("Miniconf says: %s", msg); break;
  default: ::Info("Miniconf says: %s",  msg);  break;
  }
}

//____________________________________________________________________
inline bool 
MINICONF_client::SendCommand(const std::string& args)
{
  std::cout << "Sending command " << _goName << " with arguments '" 
	    << args << "'" << std::endl;
  int ret = DimClient::sendCommand(_goName.c_str(), args.c_str());
  return ret == 0;
}
//____________________________________________________________________
inline const char* 
MINICONF_client::MakeStateName(const std::string& server)
{
  static std::string name;
  std::stringstream s;
  s << "MINICONF_" << server << "/STATE";
  name = s.str();
  return name.c_str();
}
//____________________________________________________________________
inline const char* 
MINICONF_client::MakeMessageName(const std::string& server)
{
  static std::string name;
  std::stringstream s;
  s << "MINICONF_" << server << "/MSG";
  name = s.str();
  return name.c_str();
}
//____________________________________________________________________
inline const char* 
MINICONF_client::MakeCommandName(const std::string& server)
{
  static std::string name;
  std::stringstream s;
  s << "MINICONF_" << server << "/GO";
  name = s.str();
  return name.c_str();
}
#endif
//____________________________________________________________________
//
// EOF
//
