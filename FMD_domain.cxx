#include <iostream>
#ifdef USE_INFOLOGGER
# define LOG_FACILITY "FMD-DCS"
extern "C" {
# include </date/infoLogger/infoLogger.h>
}
#else
# define INFO(X)  do { std::cout << "I: " << (X) << std::endl; } while (false)
# define ERROR(X) do { std::cerr << "E: " << (X) << std::endl; } while (false)
# define FATAL(X) do { std::cerr << "F: " << (X) << std::endl; } while (false)
#endif
#include <cstdarg>
#include <cstdio>
void Msg(int type, const char* format, va_list ap)
{
  static char buf[1024];
  vsnprintf(buf, 1024, format, ap);
  switch (type) { 
  case 2: ERROR(buf); break;
  case 3: FATAL(buf); break;
  default: INFO(buf); break;
  }
}
void Info(const char* msg, ...) 
{ 
  va_list ap;
  va_start(ap, msg);
  Msg(1, msg, ap);
  va_end(ap);
}
void Error(const char* msg, ...) 
{ 
  va_list ap;
  va_start(ap, msg);
  Msg(2, msg, ap);
  va_end(ap);
}
void Fatal(const char* msg, ...) 
{ 
  va_list ap;
  va_start(ap, msg);
  Msg(3, msg, ap);
  va_end(ap);
}
#include "FMD_domain.h"
#define ONLY_IMPLEMENTATION
#include "RCU_client.h"
#include "MINICONF_client.h"


//____________________________________________________________________
int
main(int argc, char** argv)
{
  DimClient::setDnsNode("localhost");

  Info("Starting FMD DCS proxy");
  FMD_domain d("FMD-FEE_0_0_0", "FMD", "FMD");
  while (true) pause();
  Info("Stopping FMD DCS proxy");
  return 0;
}
//____________________________________________________________________
//
// EOF
//

