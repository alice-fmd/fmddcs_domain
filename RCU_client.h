// -*- mode: C++ -*-
#ifndef RCU_CLIENT_H
#define RCU_CLIENT_H
#include <dim/dic.hxx>
#include <string>
class FMD_domain;

//____________________________________________________________________
class RCU_client : public DimInfoHandler
{
 public:
  enum State_t {
    NoLink = 0,
    Idle,
    Standby,
    Downloading,
    Ready,
    Running,
    Error
  };
  RCU_client(const std::string& server, FMD_domain& domain);
  const char* State2Text();
  State_t State() const { return _value; }
  void infoHandler();
protected:
  static const char* MakeStateName(const std::string& server);
  DimStampedInfo _state;
  State_t        _value;
  FMD_domain&    _domain;
};

//____________________________________________________________________
#elif defined(ONLY_IMPLEMENTATION)
#include <sstream>
#include "FMD_domain.h"

//____________________________________________________________________
inline
RCU_client::RCU_client(const std::string& server, FMD_domain& domain)
  : _state(MakeStateName(server), 0, 0, 0, this), 
    _domain(domain)
{
}
//____________________________________________________________________
const char* 
RCU_client::State2Text()
{
  switch (_value) {
  case Idle:        return "IDLE";
  case Standby:     return "STANDBY";
  case Downloading: return "DOWNLOADING";
  case Ready:       return "READY";
  case Running:     return "RUNNING";
  case Error:       return "ERROR";
  }
  return "No link";
}
//____________________________________________________________________
void 
RCU_client::infoHandler()
{
  DimInfo* info = getInfo();
  if (!info || info != &_state) return;

  void*    data   = info->getData();
  int      size   = info->getSize();
  bool     nolink =  (!data || size <= 0);
  int      val    = NoLink;
  if (!nolink) memcpy(&val, data, sizeof(int));

  switch (val) { 
  case NoLink:      _value = NoLink;      break;
  case Idle:        _value = Idle;        break;
  case Standby:     _value = Standby;     break;
  case Downloading: _value = Downloading; break;
  case Ready:       _value = Ready;       break;
  case Running:     _value = Running;     break;
  case Error:       _value = Error;       break;
  }    
    
  _domain.Update();
}
//____________________________________________________________________
inline const char* 
RCU_client::MakeStateName(const std::string& server)
{
  static std::string name;
  std::stringstream s;
  s << server << "_RCU_STATE";
  name = s.str();
  return name.c_str();
}

#endif
//____________________________________________________________________
//
// EOF
//
